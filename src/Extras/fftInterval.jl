# This files overloads functions defined
# in ApproxFun/src/Extras/fftGeneric.jl
# and FastTransforms/src/fftBigFloat.jl
# to make FFT work on intervals
# This code is mainly duplicated from the original files
# by Sheehan Olver, Mikael Slevinsky and other co-authors


## # this patch seems necessary for FFT to work on BigFloats
function Base.complex(u::Vector{T}, v::Vector{T}) where {T<:Real}
    n = length(u)
    if length(v) != n
        error("Incompatible lengths")
    end
    return [complex(u[i], v[i]) for i=1:n]
end


## adapted from FastTransforms/src/fftBigFloat.jl

# AbstractInterval already defined in IntervalArithmetic
#const AbstractInterval = Interval{T} where T<:AbstractFloat
const AbstractIntervals = Union{IT,Complex{IT}} where IT<:AbstractInterval
const RealIntervals = IT where IT<:AbstractInterval
const ComplexIntervals = Complex{IT} where IT<:AbstractInterval


function generic_fft(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (ret = generic_fft(x))
    ret
end

function generic_fft!(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (x[:] .= generic_fft(x))
    x
end

function generic_fft(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (ret = generic_fft(x))
    ret
end

function generic_fft!(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (x[:] .= generic_fft(x))
    x
end

function generic_fft(x::StridedMatrix{IT}, region::Integer) where IT<:AbstractIntervals
    if region == 1
        ret = hcat([generic_fft(x[:, j]) for j in 1:size(x, 2)]...)
    end
    ret
end

function generic_fft!(x::StridedMatrix{IT}, region::Integer) where IT<:AbstractIntervals
    if region == 1
        for j in 1:size(x, 2)
            x[:, j] .= generic_fft(x[:, j])
        end
    end
    x
end

function generic_fft(x::Vector{IT}) where IT<:AbstractIntervals
    IT <: FFTW.fftwNumber && (@warn("Using generic fft for FFTW number type."))
    n = length(x)
    ispow2(n) && return generic_fft_pow2(x)
    ks = range(zero(real(IT)),stop=n-one(real(IT)),length=n)
    Wks = exp.((-im).*convert(IT,π).*ks.^2 ./ n)
    xq, wq = x.*Wks, conj([exp(-im*convert(IT,π)*n);reverse(Wks);Wks[2:end]])
    return Wks.*_conv!(xq,wq)[n+1:2n]
end

generic_bfft(x::StridedArray{IT, N}, region) where {IT <: AbstractIntervals, N} = conj!(generic_fft(conj(x), region))
generic_bfft!(x::StridedArray{IT, N}, region) where {IT <: AbstractIntervals, N} = conj!(generic_fft!(conj!(x), region))
generic_ifft(x::StridedArray{IT, N}, region) where {IT<:AbstractIntervals, N} = ldiv!(length(x), conj!(generic_fft(conj(x), region)))
generic_ifft!(x::StridedArray{IT, N}, region) where {IT<:AbstractIntervals, N} = ldiv!(length(x), conj!(generic_fft!(conj!(x), region)))

generic_rfft(v::Vector{IT}, region) where IT<:AbstractIntervals = generic_fft(v, region)[1:div(length(v),2)+1]
function generic_irfft(v::Vector{IT}, n::Integer, region) where IT<:ComplexIntervals
    @assert n==2length(v)-1
    r = Vector{IT}(undef, n)
    r[1:length(v)]=v
    r[length(v)+1:end]=reverse(conj(v[2:end]))
    real(generic_ifft(r, region))
end
generic_brfft(v::StridedArray, n::Integer, region) = generic_irfft(v, n, region)*n

function _conv!(u::StridedVector{IT}, v::StridedVector{IT}) where IT<:AbstractIntervals
    nu = length(u)
    nv = length(v)
    n = nu + nv - 1
    np2 = nextpow(2, n)
    append!(u, zeros(IT, np2-nu))
    append!(v, zeros(IT, np2-nv))
    y = generic_ifft_pow2(generic_fft_pow2(u).*generic_fft_pow2(v))
    #TODO This would not handle Dual/ComplexDual numbers correctly
    y = IT<:Real ? real(y[1:n]) : y[1:n]
end

conv(u::AbstractArray{IT, N}, v::AbstractArray{IT, N}) where {IT<:AbstractInterval, N} = _conv!(deepcopy(u), deepcopy(v))
conv(u::AbstractArray{IT, N}, v::AbstractArray{Complex{IT}, N}) where {IT<:AbstractInterval, N} = _conv!(complex(deepcopy(u)), deepcopy(v))
conv(u::AbstractArray{Complex{IT}, N}, v::AbstractArray{IT, N}) where {IT<:AbstractInterval, N} = _conv!(deepcopy(u), complex(deepcopy(v)))
conv(u::AbstractArray{Complex{IT}, N}, v::AbstractArray{Complex{IT}, N}) where {IT<:AbstractInterval, N} = _conv!(deepcopy(u), deepcopy(v))

function generic_fft_pow2!(x::Vector{IT}) where IT<:AbstractInterval
    n,big2=length(x),2one(IT)
    nn,j=n÷2,1
    for i=1:2:n-1
        if j>i
            x[j], x[i] = x[i], x[j]
            x[j+1], x[i+1] = x[i+1], x[j+1]
        end
        m = nn
        while m ≥ 2 && j > m
            j -= m
            m = m÷2
        end
        j += m
    end
    logn = 2
    while logn < n
        θ=-big2/logn
        wtemp = sinpi(θ/2)
        wpr, wpi = -2wtemp^2, sinpi(θ)
        wr, wi = one(IT), zero(IT)
        for m=1:2:logn-1
            for i=m:2logn:n
                j=i+logn
                mixr, mixi = wr*x[j]-wi*x[j+1], wr*x[j+1]+wi*x[j]
                x[j], x[j+1] = x[i]-mixr, x[i+1]-mixi
                x[i], x[i+1] = x[i]+mixr, x[i+1]+mixi
            end
            wr = (wtemp=wr)*wpr-wi*wpi+wr
            wi = wi*wpr+wtemp*wpi+wi
        end
        logn = logn << 1
    end
    return x
end

function generic_fft_pow2(x::Vector{Complex{IT}}) where IT<:AbstractInterval
    y = interlace(real(x), imag(x))
    generic_fft_pow2!(y)
    return complex.(y[1:2:end], y[2:2:end])
end
generic_fft_pow2(x::Vector{IT}) where IT<:AbstractInterval = generic_fft_pow2(complex(x))

function generic_ifft_pow2(x::Vector{Complex{IT}}) where IT<:AbstractInterval
    y = interlace(real(x), -imag(x))
    generic_fft_pow2!(y)
    return ldiv!(length(x), conj!(complex.(y[1:2:end], y[2:2:end])))
end

function generic_dct(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (ret = generic_dct(x))
    ret
end

function generic_dct!(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (x[:] .= generic_dct(x))
    x
end

function generic_idct(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (ret = generic_idct(x))
    ret
end

function generic_idct!(x::StridedVector{IT}, region::Integer) where IT<:AbstractIntervals
    region == 1 && (x[:] .= generic_idct(x))
    x
end

function generic_dct(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (ret = generic_dct(x))
    ret
end

function generic_dct!(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (x[:] .= generic_dct(x))
    x
end

function generic_idct(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (ret = generic_idct(x))
    ret
end

function generic_idct!(x::StridedVector{IT}, region::UnitRange{I}) where {IT<:AbstractIntervals, I<:Integer}
    region == 1:1 && (x[:] .= generic_idct(x))
    x
end

function generic_dct(a::AbstractVector{Complex{IT}}) where {IT <: AbstractInterval}
    IT <: FFTW.fftwNumber && (@warn("Using generic dct for FFTW number type."))
    N = length(a)
    twoN = convert(IT,2) * N
    c = generic_fft([a; reverse(a, dims=1)]) # c = generic_fft([a; flipdim(a,1)])
    d = c[1:N]
    d .*= exp.((-im*convert(IT, pi)).*(0:N-1)./twoN)
    d[1] = d[1] / sqrt(convert(IT, 2))
    lmul!(inv(sqrt(twoN)), d)
end

generic_dct(a::AbstractArray{IT}) where {IT <: AbstractInterval} = real(generic_dct(complex(a)))

function generic_idct(a::AbstractVector{Complex{IT}}) where {IT <: AbstractInterval}
    IT <: FFTW.fftwNumber && (@warn("Using generic idct for FFTW number type."))
    N = length(a)
    twoN = convert(IT,2)*N
    b = a * sqrt(twoN)
    b[1] = b[1] * sqrt(convert(IT,2))
    shift = exp.(-im * 2 * convert(IT, pi) * (N - convert(IT,1)/2) * (0:(2N-1)) / twoN)
    b = [b; 0; -reverse(b[2:end], dims=1)] .* shift # b = [b; 0; -flipdim(b[2:end],1)] .* shift
    c = ifft(b)
    reverse(c[1:N]; dims=1)#flipdim(c[1:N],1)
end

generic_idct(a::AbstractArray{IT}) where {IT <: AbstractInterval} = real(generic_idct(complex(a)))


# These lines mimick the corresponding ones in FFTW/src/dct.jl, but with
# IAbstractFloat rather than fftwNumber.
for f in (:dct, :dct!, :idct, :idct!)
    pf = Symbol("plan_", f)
    @eval begin
        $f(x::AbstractArray{<:AbstractIntervals}) = $pf(x) * x
        $f(x::AbstractArray{<:AbstractIntervals}, region) = $pf(x, region) * x
    end
end

# dummy plans
abstract type DummyPlan{IT} <: Plan{IT} end
for P in (:DummyFFTPlan, :DummyiFFTPlan, :DummybFFTPlan, :DummyDCTPlan, :DummyiDCTPlan)
    # All plans need an initially undefined pinv field
    @eval begin
        mutable struct $P{IT,inplace,G} <: DummyPlan{IT}
            region::G # region (iterable) of dims that are transformed
            pinv::DummyPlan{IT}
            $P{IT,inplace,G}(region::G) where {IT<:AbstractIntervals, inplace, G} = new(region)
        end
    end
end
for P in (:DummyrFFTPlan, :DummyirFFTPlan, :DummybrFFTPlan)
    @eval begin
        mutable struct $P{IT,inplace,G} <: DummyPlan{IT}
            n::Integer
            region::G # region (iterable) of dims that are transformed
            pinv::DummyPlan{IT}
            $P{IT,inplace,G}(n::Integer, region::G) where {IT<:AbstractIntervals, inplace, G} = new(n, region)
        end
    end
end

for (Plan,iPlan) in ((:DummyFFTPlan,:DummyiFFTPlan),
                     (:DummyDCTPlan,:DummyiDCTPlan))
   @eval begin
       plan_inv(p::$Plan{IT,inplace,G}) where {IT,inplace,G} = $iPlan{IT,inplace,G}(p.region)
       plan_inv(p::$iPlan{IT,inplace,G}) where {IT,inplace,G} = $Plan{IT,inplace,G}(p.region)
    end
end

# Specific for rfft, irfft and brfft:
plan_inv(p::DummyirFFTPlan{IT,inplace,G}) where {IT,inplace,G} = DummyrFFTPlan{IT,Inplace,G}(p.n, p.region)
plan_inv(p::DummyrFFTPlan{IT,inplace,G}) where {IT,inplace,G} = DummyirFFTPlan{IT,Inplace,G}(p.n, p.region)



for (Plan,ff,ff!) in ((:DummyFFTPlan,:generic_fft,:generic_fft!),
                      (:DummybFFTPlan,:generic_bfft,:generic_bfft!),
                      (:DummyiFFTPlan,:generic_ifft,:generic_ifft!),
                      (:DummyrFFTPlan,:generic_rfft,:generic_rfft!),
                      (:DummyDCTPlan,:generic_dct,:generic_dct!),
                      (:DummyiDCTPlan,:generic_idct,:generic_idct!))
    @eval begin
        *(p::$Plan{IT,true}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = $ff!(x, p.region)
        *(p::$Plan{IT,false}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = $ff(x, p.region)
        function mul!(C::StridedVector, p::$Plan, x::StridedVector)
            C[:] = $ff(x, p.region)
            C
        end
    end
end

# Specific for irfft and brfft:
*(p::DummyirFFTPlan{IT,true}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = generic_irfft!(x, p.n, p.region)
*(p::DummyirFFTPlan{IT,false}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = generic_irfft(x, p.n, p.region)
function mul!(C::StridedVector, p::DummyirFFTPlan, x::StridedVector)
    C[:] = generic_irfft(x, p.n, p.region)
    C
end
*(p::DummybrFFTPlan{IT,true}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = generic_brfft!(x, p.n, p.region)
*(p::DummybrFFTPlan{IT,false}, x::StridedArray{IT,N}) where {IT<:AbstractIntervals,N} = generic_brfft(x, p.n, p.region)
function mul!(C::StridedVector, p::DummybrFFTPlan, x::StridedVector)
    C[:] = generic_brfft(x, p.n, p.region)
    C
end


AbstractFFTs.complexfloat(x::StridedArray{Complex{<:AbstractInterval}}) = x
# We override this one in order to avoid throwing an error that the type is
# unsupported (as defined in AbstractFFTs)
AbstractFFTs._fftfloat(::Type{IT}) where {IT <: AbstractInterval} = IT

plan_fft(x::StridedArray{IT}, region) where {IT <: ComplexIntervals} = DummyFFTPlan{Complex{real(IT)},false,typeof(region)}(region)
plan_fft!(x::StridedArray{IT}, region) where {IT <: ComplexIntervals} = DummyFFTPlan{Complex{real(IT)},true,typeof(region)}(region)

plan_bfft(x::StridedArray{IT}, region) where {IT <: ComplexIntervals} = DummybFFTPlan{Complex{real(IT)},false,typeof(region)}(region)
plan_bfft!(x::StridedArray{IT}, region) where {IT <: ComplexIntervals} = DummybFFTPlan{Complex{real(IT)},true,typeof(region)}(region)

plan_dct(x::StridedArray{IT}, region) where {IT <: AbstractIntervals} = DummyDCTPlan{IT,false,typeof(region)}(region)
plan_dct!(x::StridedArray{IT}, region) where {IT <: AbstractIntervals} = DummyDCTPlan{IT,true,typeof(region)}(region)

plan_idct(x::StridedArray{IT}, region) where {IT <: AbstractIntervals} = DummyiDCTPlan{IT,false,typeof(region)}(region)
plan_idct!(x::StridedArray{IT}, region) where {IT <: AbstractIntervals} = DummyiDCTPlan{IT,true,typeof(region)}(region)

plan_rfft(x::StridedArray{IT}, region) where {IT <: RealIntervals} = DummyrFFTPlan{Complex{real(IT)},false,typeof(region)}(length(x), region)
plan_brfft(x::StridedArray{IT}, n::Integer, region) where {IT <: ComplexIntervals} = DummybrFFTPlan{Complex{real(IT)},false,typeof(region)}(n, region)


## adapted from ApproxFun/src/Extras/fftGeneric.jl

plan_transform(sp::Fourier{D,R},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R} =
    TransformPlan{IT,typeof(sp),false,Nothing}(sp,nothing)
plan_itransform(sp::Fourier{D,R},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R} = begin
    ITransformPlan{IT,typeof(sp),false,Nothing}(sp,nothing)
end

function *(::TransformPlan{IT,Fourier{D,R},false},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R}
    l = length(x); n = div(l+1,2)
    v = fft(x)
    rmul!(v,convert(IT,2)/l)
    v[1] /= 2
    mod(l,2) == 1 ? ApproxFunBase.interlace(real(v[1:n]),-imag(v[2:n])) :
      [ApproxFunBase.interlace(real(v[1:n]),-imag(v[2:n]));-real(v[n+1])/2]
end

function *(::ITransformPlan{IT,Fourier{D,R},false},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R}
    l = length(x); n = div(l+1,2)
    # v = complex([x[1:n];x[n-1:-1:2]],[0;-x[2n-2:-1:n+1];0;x[n+1:2n-2]])
    v = mod(l,2) == 1 ?
        complex([2x[1];x[3:2:end];x[end:-2:3]],[0;-x[2:2:end];x[end-1:-2:2]]) :
        complex([2x[1];x[3:2:end-1];-2x[end];x[end-1:-2:3]],[0;-x[2:2:end];x[end-2:-2:2]])
    rmul!(v,convert(IT,l)/2)
     real(ifft(v))
end

# SinSpace plans for FFTnpNumber types

plan_transform(sp::SinSpace{D,R},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R} =
    TransformPlan{IT,typeof(sp),false,Nothing}(sp,nothing)
plan_itransform(sp::SinSpace{D,R},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R} =
    ITransformPlan{IT,typeof(sp),false,Nothing}(sp,nothing)


function *(::TransformPlan{IT,SinSpace{D,R},false},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R}
    v=imag(fft([0;-x;0;reverse(x)]))[2:length(x)+1]
    rmul!(v,convert(IT,1)/(length(x)+1))
    v
end

*(::ITransformPlan{IT,SinSpace{D,R},false},x::AbstractVector{IT}) where {IT<:AbstractInterval,D,R} =
    imag(fft([0;-x;0;reverse(x)]))[2:length(x)+1]/2


# Fourier space & SinSpace plans for complex Intervals

for SP in (:Fourier,:SinSpace), (pl,TransPlan) in ((:plan_transform,:TransformPlan),
                                                    (:plan_itransform,:ITransformPlan))
    @eval begin
        $pl(sp::$SP{D,R},x::AbstractVector{IT}) where {IT<:ComplexIntervals,D,R} =
                $TransPlan(sp,$pl(sp,Array{IT}(undef, length(x))),Val{false})
        *(P::$TransPlan{IT,$SP{D,R},false},x::Vector{IT}) where {IT<:ComplexIntervals,D,R} =
            complex(P.plan*real(x),P.plan*imag(x))
    end
end
