
module RigorousFourier

using LinearAlgebra, IntervalArithmetic, ApproxFun, FastTransforms
import IntervalArithmetic: Interval, @round, @round_up, @round_down, mid
import ApproxFun: (..), resize!, evaluate, +, -, *, /, ^, sum, cumsum
import ApproxFun.AbstractFFTs: Plan, ScaledPlan,
                     fft, ifft, bfft, fft!, ifft!, bfft!, rfft, irfft, brfft,
                     plan_fft, plan_ifft, plan_bfft, plan_fft!, plan_ifft!,
                     plan_bfft!, plan_rfft, plan_irfft, plan_brfft,
                     fftshift, ifftshift, rfft_output_size, brfft_output_size,
                     plan_inv, normalization
import ApproxFun.FFTW: dct, dct!, idct, idct!, plan_dct!, plan_idct!,
             plan_dct, plan_idct, fftwNumber
import ApproxFun.ApproxFunBase: TransformPlan, ITransformPlan, plan_transform, plan_itransform, interlace

export TA, ITA, RTA, mid, discretebound, wnorm, resize!, +, -, *, /, ^, sum, cumsum, in_nonzero, is_positive, is_negative


# some patches to ApproxFun
include("Extras/Extras.jl")



# default number of interpolation points
default_len = 100
function set_default_len(len)
    global default_len = len
    return nothing
end

# if true, use FFT for fast multiplication of RTAs
default_RTA_fastmul = false
# if FFT based multiplication for RTAs, use intervals or non rigorous floating-point numbers?
default_RTA_fastmul_interval = true
# systematically use a power of 2 for FFT based operations, to improve efficiency
default_usepow2 = false
function set_default_RTA_fastmul(flag::Bool)
    global default_RTA_fastmul = flag
    return nothing
end
function set_default_RTA_fastmul_interval(flag::Bool)
    global default_RTA_fastmul_interval = flag
    return nothing
end
function set_default_usepow2(flag::Bool)
    global default_usepow2 = flag
    return nothing
end


# Fourier Space with coefficients in T
const FStype = ApproxFunBase.SumSpace{Tuple{CosSpace{PeriodicSegment{T},T},SinSpace{PeriodicSegment{T},T}},PeriodicSegment{T},T} where {T<:Real}
FS(::Type{T}) where {T<:Real} = Fourier(T(0)..2*T(π))
grid(::Type{T}; len=default_len) where {T<:Real} = points(FS(T), len)
# Fourier Space with interval coefficients with endpoints in T
const IFStype = ApproxFunBase.SumSpace{Tuple{CosSpace{PeriodicSegment{Interval{T}},Interval{T}},SinSpace{PeriodicSegment{Interval{T}},Interval{T}}},PeriodicSegment{Interval{T}},Interval{T}} where {T<:Real}
IFS(::Type{T}) where {T<:Real} = Fourier(Interval{T}(0)..2*Interval{T}(π))


# function types

# type for Trigonometric Approximations (TA)
const TA = Fun{FStype{T},T,Vector{T}} where {T<:Real}

# TA from T coefficients
TA{T}(cfs::Vector{T}) where {T<:Real} = Fun(FS(T), cfs)
TA(cfs::Vector{T}) where {T<:Real} = TA{T}(cfs)

# TA from function by interpolation
TA{T}(f::Function; len=default_len) where {T<:Real} = TA(ApproxFun.transform(FS(T), f.(grid(T, len=len))))


# type for Interval Trigonometric Approximations (ITA)
const ITA = Fun{FStype{Interval{T}},Interval{T},Vector{Interval{T}}} where {T<:Real}

# ITA from Interval{T} coefficients
ITA{T}(cfs::Vector{Interval{T}}) where {T<:Real} = Fun(IFS(T), cfs)
ITA(cfs::Vector{Interval{T}}) where {T<:Real} = ITA{T}(cfs)

# ITA from T coefficients
ITA(cfs::Vector{T}) where {T<:Real} = ITA([Interval{T}(c) for c=cfs])

# ITA from TA
ITA(f::TA{T}) where {T<:Real} = ITA(coefficients(f))

# prevent ApproxFun from truncating rigorous series automatically and nonrigorously
ApproxFunBase.chop!(sp::ApproxFunBase.UnivariateSpace,cfs::Vector{Interval{T}},tol::Real) where {T<:Real} = cfs

# type for Rigorous Trigonometric Approximations (RTA)
mutable struct RTA{T<:Real}
    pol::ITA{T}
    rem::T
    RTA{T}(pol::ITA{T}, rem::T) where {T<:Real} = rem < 0 ? error("RTA with negative remainder") : new(pol, rem)
end

# RTA from ITA and rem
RTA(pol::ITA{T}, rem::T) where {T<:Real} = RTA{T}(pol, rem)

# RTA from Interval{T} coefficients / T coefficients / TA and T rem
RTA(cfs::Vector{T}, rem::T) where {T<:Real} = RTA(ITA(cfs), rem)
RTA(cfs::Vector{Interval{T}}, rem::T) where {T<:Real} = RTA(ITA(cfs), rem)
RTA(pol::TA{T}, rem::T) where {T<:Real} = RTA(ITA(pol), rem)

# RTA defined without remainder
RTA(cfs::Vector{Interval{T}}) where {T<:Real} = RTA(cfs, T(0))
RTA(cfs::Vector{T}) where {T<:Real} = RTA(cfs, T(0))
RTA(f::ITA{T}) where {T<:Real} = RTA(f, T(0))
RTA(f::TA{T}) where {T<:Real} = RTA(f, T(0))

# TA from ITA by taking coefficient middles
mid(f::ITA{T}) where {T<:Real} = TA(mid.(coefficients(f)))
TA(f::ITA{T}) where {T<:Real} = mid(f)

# TA from RTA by taking coefficient middles and discarding remainder
mid(f::RTA{T}) where {T<:Real} = mid(f.pol)
TA(f::RTA{T}) where {T<:Real} = mid(f)

# evaluation of RTA
evaluate(f::RTA{T}, x::Interval{T}) where {T<:Real} = f.pol(x) + f.rem * Interval{T}(-1,1)

(f::RTA{T})(x...) where {T<:Real} = evaluate(f, x...)

evaluate(f::RTA{T}, x::T) where {T<:Real}= f(Interval{T}(x))


# bounds

# sup norm taken on the grid
function discretebound(f::TA{T}) where {T<:Real}
    vals = ApproxFun.itransform(FS(T), coefficients(f))
    return maximum(abs.(vals))
end

# Wiener bound (sum of absolute values of coefficients)

# rigorous Wiener norm of TA
function wnorm(f::TA{T}) where {T<:Real}
    bnd = T(0)
    cfs = coefficients(f)
    for c in cfs
        bnd = @round_up(bnd + abs(c))
    end
    return bnd
end

# rigorous Wiener norm of ITA
function wnorm(f::ITA{T}) where {T<:Real}
    bnd = T(0)
    cfs = coefficients(f)
    for c in cfs
        bnd = @round_up(bnd + abs(c).hi)
    end
    return bnd
end

# rigorous Wiener norm of RTA
wnorm(f::RTA{T}) where {T<:Real} = @round_up(wnorm(f.pol) + f.rem)

# resize

# resize TA by discarding last coefficients
# if pad=true, zero-padding
function resize!(f::TA{T}, cutlen=default_len; pad=false) where {T<:Real}
    cfs = coefficients(f)
    len = length(cfs)
    if len > cutlen || pad
        pad!(f, cutlen)
    end
    return f
end

# resize RTA rigorously
# if pad=true, zero-padding
function resize!(f::RTA{T}, cutlen=default_len; pad=false) where {T<:Real}
    cfs = coefficients(f.pol)
    len = length(cfs)
    if len > cutlen
        for i in cutlen+1:len
            f.rem = @round_up(f.rem + abs(cfs[i]).hi)
        end
        resize!(cfs, cutlen)
    elseif pad && len < cutlen
        pad!(cfs, cutlen)
    end
    return f
end


# flatten interval coefficients
# returns floating-point coefficients, and
# a rigorous bound on the cumulated error
function flatten(f::ITA{T}) where {T<:Real}
    cfs = coefficients(f)
    f0 = TA(mid.(cfs))
    ϵ = wnorm(f - ITA(f0))
    return f0, ϵ
end

function flatten(f::RTA{T}) where {T<:Real}
    f0, ϵ = flatten(f.pol)
    return RTA(f0, @round_up (f.rem + ϵ))
end

function flatten!(f::RTA{T}) where {T<:Real}
    f0, ϵ = flatten(f.pol)
    f.pol = ITA(f0)
    f.rem = @round_up(f.rem + ϵ)
    return nothing
end

# arithmetic operations

# unary operations

+(f::RTA{T}) where {T<:Real} = RTA(+f.pol, f.rem)

-(f::RTA{T}) where {T<:Real} = RTA(-f.pol, f.rem)

# binary operations
# unfortunately / and * are quadratic for ITA/RTA

+(f::TA{T}, c::U) where {T,U<:Real} = f + TA([T(c)])
+(c::U, f::TA{T}) where {T,U<:Real} = TA([T(c)]) + f

+(f::ITA{T}, c::U) where {T,U<:Real} = f + ITA([Interval{T}(c)])
+(c::U, f::ITA{T}) where {T,U<:Real} = ITA([Interval{T}(c)]) + f
+(f::ITA{T}, g::TA{T}) where {T<:Real} = f + ITA(g)
+(f::TA{T}, g::ITA{T}) where {T<:Real} = ITA(f) + g

+(f::RTA{T}, g::RTA{T}; len=default_len) where {T<:Real} = resize!(RTA(f.pol + g.pol, @round_up(f.rem + g.rem)), len)

+(f::RTA{T}, c::U) where {T,U<:Real} = f + RTA([Interval{T}(c)])
+(c::U, f::RTA{T}) where {T,U<:Real} = RTA([Interval{T}(c)]) + f
+(f::RTA{T}, g::Union{TA{T},ITA{T}}) where {T<:Real} = f + RTA(g)
+(f::Union{TA{T},ITA{T}}, g::RTA{T}) where {T<:Real} = RTA(f) + g

-(f::TA{T}, c::U) where {T,U<:Real} = f - TA([T(c)])
-(c::U, f::TA{T}) where {T,U<:Real} = TA([T(c)]) - f

-(f::ITA{T}, c::U) where {T,U<:Real} = f - ITA([Interval{T}(c)])
-(c::U, f::ITA{T}) where {T,U<:Real} = ITA([Interval{T}(c)]) - f
-(f::ITA{T}, g::TA{T}) where {T<:Real} = f - ITA(g)
-(f::TA{T}, g::ITA{T}) where {T<:Real} = ITA(f) - g

-(f::RTA{T}, g::RTA{T}) where {T<:Real} = RTA(f.pol - g.pol, @round_up(f.rem + g.rem))

-(f::RTA{T}, c::U) where {T,U<:Real} = f - RTA([Interval{T}(c)])
-(c::U, f::RTA{T}) where {T,U<:Real} = RTA([Interval{T}(c)]) - f
-(f::RTA{T}, g::Union{TA{T},ITA{T}}) where {T<:Real} = f - RTA(g)
-(f::Union{TA{T},ITA{T}}, g::RTA{T}) where {T<:Real} = RTA(f) - g


function *(f::TA{T}, g::TA{T}; len=default_len) where {T<:Real}
    h = ApproxFunBase.default_mult(f, g)
    resize!(h, len)
    return h
end

*(c::U, f::TA{T}; len=default_len) where {T,U<:Real} = resize!(TA(T(c) * coefficients(f)), len)
*(f::TA{T}, c::U; len=default_len) where {T,U<:Real} = resize!(TA(T(c) * coefficients(f)), len)

function *(f::ITA{T}, g::ITA{T}) where {T<:Real}
    n = ncoefficients(f)
    m = ncoefficients(g)
    if n == 0 || m == 0
        return ITA(Vector{Interval{T}}())
    elseif n == 1
        return coefficients(f)[1] * g
    elseif m == 1
        return coefficients(g)[1] * f
    else
        return ApproxFunBase.coefficienttimes(f, g)
    end
end
*(c::U, f::ITA{T}) where {T,U<:Real} = ITA(Interval{T}(c) * coefficients(f))
*(f::ITA{T}, c::U) where {T,U<:Real} = ITA(Interval{T}(c) * coefficients(f))
*(f::TA{T}, g::ITA{T}) where {T<:Real} = ITA(f) * g
*(f::ITA{T}, g::TA{T}) where {T<:Real} = f * ITA(g)

function *(f::RTA{T}, g::RTA{T}; len=default_len, fastmul=default_RTA_fastmul, fastmul_interval=default_RTA_fastmul_interval, usepow2=default_usepow2) where {T<:Real}
    n = ncoefficients(f.pol) ÷ 2
    m = ncoefficients(g.pol) ÷ 2
    if fastmul && n > 0 && m > 0 && log10(n) * log10(m) > 2
        # flatten before using FFT
        f0 = flatten(f)
        g0 = flatten(g)
        remf = f0.rem
        remg = g0.rem
        polfnorm = wnorm(f0.pol)
        polgnorm = wnorm(g0.pol)
        # perform FFT multiplication
        N = 2*(n+m)+1
        N0 = usepow2 ? nextpow(2, N) : N
        resize!(f0, N0, pad=true)
        resize!(g0, N0, pad=true)
        if fastmul_interval
            FSIT = FS(Interval{T})
            f0pts = ApproxFun.itransform(FSIT, coefficients(f0.pol))
            g0pts = ApproxFun.itransform(FSIT, coefficients(g0.pol))
            h0 = ApproxFun.transform(FSIT, f0pts .* g0pts)[1:N]
        else
            FST = FS(T)
            f0pts = ApproxFun.itransform(FST, T.(coefficients(f0.pol)))
            g0pts = ApproxFun.itransform(FST, T.(coefficients(g0.pol)))
            h0 = ApproxFun.transform(FST, f0pts .* g0pts)[1:N]
        end
        h = RTA(h0)
    else
        h = RTA(f.pol * g.pol)
        remf = f.rem
        remg = g.rem
        polfnorm = wnorm(f.pol)
        polgnorm = wnorm(g.pol)
    end
    resize!(h, len)
    h.rem = @round_up(h.rem + @round_up(remf * polgnorm))
    h.rem = @round_up(h.rem + @round_up(remg * polfnorm))
    h.rem = @round_up(h.rem + @round_up(remf * remg))
    return h
end

function *(c::U, f::RTA{T}; len=default_len) where {T,U<:Real}
    ITc = Interval{T}(c)
    return resize!(RTA(ITc * f.pol, @round_up(abs(ITc).hi * f.rem)), len)
end
function *(f::RTA{T}, c::U; len=default_len) where {T,U<:Real}
    ITc = Interval{T}(c)
    return resize!(RTA(ITc * f.pol, @round_up(abs(ITc).hi * f.rem)), len)
end
*(f::Union{TA{T}, ITA{T}}, g::RTA{T}; len=default_len, usepow2=default_usepow2) where {T,U<:Real} = *(RTA(f), g, len=len, usepow2=usepow2)
*(f::RTA{T}, g::Union{TA{T}, ITA{T}}; len=default_len, usepow2=default_usepow2) where {T<:Real} = *(f, RTA(g), len=len, usepow2=usepow2)

function /(f::TA{T}, g::TA{T}; len=default_len, usepow2=default_usepow2) where {T<:Real}
    ff = TA(copy(coefficients(f)))
    gg = TA(copy(coefficients(g)))
    n = usepow2 ? nextpow(2, len) : len
    resize!(ff, n, pad=true)
    resize!(gg, n, pad=true)
    FST = FS(T)
    fvals = ApproxFun.itransform(FST, coefficients(ff))
    gvals = ApproxFun.itransform(FST, coefficients(gg))
    h = ApproxFun.transform(FST, fvals ./ gvals)[1:len]
    return TA(h)
end

/(c::U, f::TA{T}; len=default_len, usepow2=default_usepow2) where {T,U<:Real} = /(TA([T(c)]), f, len=len, usepow2=usepow2)
/(f::TA{T}, c::U) where {T,U<:Real} = (U(1)/c) * f

function /(f::RTA{T}, g::RTA{T}; len=default_len, usepow2=default_usepow2) where {T<:Real}
    maxlen = max(len, default_len)
    h = /(TA(f), TA(g), len=len, usepow2=usepow2)
    w = /(1, TA(g), len=maxlen, usepow2=usepow2)
    μ = wnorm(1 - *(w, g, len=maxlen, usepow2=usepow2))
    if μ >= 1
        error("Failed to create contracting operator")
    end
    b = wnorm(*(w, (*(g, h, len=maxlen, usepow2=usepow2) - f), len=maxlen, usepow2=usepow2))
    rem = @round_up(b / @round_down(T(1)-μ))
    return RTA(h, rem)
end

/(f::RTA{T}, g::Union{TA{T}, ITA{T}}; len=default_len, usepow2=default_usepow2) where {T<:Real} = /(f, RTA(g), len=len, usepow2=usepow2)
/(f::Union{TA{T}, ITA{T}}, g::RTA{T}; len=default_len, usepow2=default_usepow2) where {T<:Real} = /(RTA(f), g, len=len, usepow2=usepow2)
/(f::ITA{T}, g::ITA{T}; len=default_len, usepow2=default_usepow2) where {T<:Real} = /(RTA(f), RTA(g), len=len, usepow2=usepow2)
/(f::ITA{T}, g::TA{T}; len=default_len, usepow2=default_usepow2) where {T<:Real} = /(RTA(f), RTA(g), len=len, usepow2=usepow2)
/(f::TA{T}, g::ITA{T}, len=default_len, usepow2=default_usepow2) where {T<:Real} = /(RTA(f), RTA(g), len=len, usepow2=usepow2)
/(c::U, f::Union{ITA{T}, RTA{T}}, len=default_len, usepow2=default_usepow2) where {T,U<:Real} = /(RTA([Interval{T}(c)]), f, len=len, usepow2=usepow2)
/(f::Union{ITA{T}, RTA{T}}, c::U) where {T,U<:Real} = f * (Interval{T}(1) / Interval{T}(c))


function ^(f::TA{T}, n::Int; len=default_len, usepow2=default_usepow2) where {T<:Real}
    if n < 0
        return /(1, ^(f, -n, len=len, usepow2=usepow2), len=len, usepow2=usepow2)
    elseif n == 0
        return TA([T(1)])
    elseif n == 1
        return deepcopy(f)
    else
        return *(f, ^(f, n-1, len=len, usepow2=usepow2), len=len)
    end
end

# it seems negative power does not work ==> Julia automatically calls "inv"
function ^(f::RTA{T}, n::Int; len=default_len, usepow2=default_usepow2) where {T<:Real}
    if n < 0
        return /(1, ^(f, -n, len=len, usepow2=usepow2), len=len, usepow2=usepow2)
    elseif n == 0
        return RTA([Interval{T}(1)])
    elseif n == 1
        return deepcopy(f)
    else
        return *(f, ^(f, n-1, len=len, usepow2=usepow2), len=len, usepow2=usepow2)
    end
end


# primitive for TA
# NOTE: does note take the constant coefficient into consideration!
function cumsum(f::TA{T}) where {T<:Real}
    n = length(coefficients(f))
    n = (n % 2 == 0) ? n+1 : n
    cfs = Vector{T}(undef, n)
    cfs[1] = T(0)
    for i = 2:n
        if (i % 2 == 0)
            cfs[i] = coefficient(f, i+1) / (i ÷ 2)
        else
            cfs[i] = -coefficient(f, i-1) / (i ÷ 2)
        end
    end
    return TA(cfs)
end

# rigorous integration of RTA over [0,2π]
sum(f::RTA{T}) where {T<:Real} = sum(f.pol) + f.rem * Interval{T}(-2, 2) * Interval{T}(π)


# non zero check
# quick check with the Wiener bound minus the constant coefficient
# if not sufficient to decide and precond=true, precondition with an approximate inverse TA
function is_nonzero(f::RTA{T}; precond=true) where {T<:Real}
    cfs = coefficients(f.pol)
    if length(cfs) == 0
        return false
    end
    cst = cfs[1]
    # simple check by comparing constant coefficient with the rest of the polynomial
    if @round_up(wnorm(f) - mag(cst)) < mig(cst)
        return true
    # otherwise, if precond=true, multiply by approximate inverse
    elseif precond
        g = TA([T(0)])
        try
            g = 1 / TA(f)
        catch
            return false
        end
        h = g * f
        return is_nonzero(h, precond=false)
    # return false otherwise, that is not able to prove that f does not vanish
    else
        return false
    end

end

is_nonzero(f::Union{TA{T}, ITA{T}}) where {T<:Real} = is_nonzero(RTA(f))

# positivity check
function is_positive(f::RTA{T}) where {T<:Real}
    cfs = coefficients(f.pol)
    if length(cfs) == 0 || cfs[1].lo <= T(0)
        return false
    else
        return is_nonzero(f)
    end
end

is_positive(f::Union{TA{T}, ITA{T}}) where {T<:Real} = is_positive(RTA(f))

# negativity check
function is_negative(f::RTA{T}) where {T<:Real}
    cfs = coefficients(f.pol)
    if length(cfs) == 0 || cfs[1].lo >= T(0)
        return false
    else
        return is_nonzero(f)
    end
end

is_negative(f::Union{TA{T}, ITA{T}}) where {T<:Real} = is_negative(RTA(f))

end # module
