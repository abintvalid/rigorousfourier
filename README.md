# RigorousFourier

Routines for Rigorous Trigonometric Approximations (RTAs), built on top of [`ApproxFun.jl`](https://github.com/JuliaApproximation/ApproxFun.jl).

This package implements the arithmetic on RTAs used in
**[[1]](#BrehardBrisebarreJoldesTucker2023)**.


## Installation

```julia
julia> ]
(v1.7) pkg> add https://gitlab.inria.fr/abintvalid/rigorousfourier
```

This package depends on:

* [`ApproxFun.jl`](https://github.com/JuliaApproximation/ApproxFun.jl) –
Package for approximating functions, in a similar vein to the Matlab package `Chebfun` and the Mathematica package `RHPackage`.

* [`FastTransforms.jl`](https://github.com/JuliaApproximation/FastTransforms.jl) –
Allows the user to conveniently work with orthogonal polynomials with degrees well into the millions.

* [`IntervalArithmetic.jl`](https://github.com/JuliaIntervals/IntervalArithmetic.jl) –
Validated Numerics in Julia, i.e. rigorous computations with finite-precision floating-point arithmetic.

* [`LinearAlgebra.jl`](https://github.com/JuliaLang/julia/tree/master/stdlib/LinearAlgebra) –
Linear algebra routines.

## Rigorous Trigonometric Approximations: Data types and operations

### Data types

* `TA{T}` – Trigonometric polynomial (or *Trigonometric Approximation*) of the form
$$p(t) = a_0 + \sum_{k=1}^N \left(a_k \cos(kt) + b_k \sin(kt)\right),$$
with numerical coefficients $a_k, b_k$ of type `T`, given as the
vector $[a_0, b_1, a_1, b_2, \dots, b_N, a_N]$.

* `ITA{T}` – *Interval Trigonometric Approximation*, i.e., a trigonometric polynomial $\boldsymbol{p}$ with interval coefficients $\boldsymbol{a_k}, \boldsymbol{b_k}$ of type `Interval{T}`.
For a trigonometric polynomial $p$ (with real coefficients), we say $p \in \boldsymbol{p}$ iff $a_k \in \boldsymbol{a_k}$
and $b_k \in \boldsymbol{b_k}$ for all $k$.

* `RTA{T}` – *Rigorous Trigonometric Approximation*, i.e., a pair
$\boldsymbol{f} = (\boldsymbol{p}, \epsilon)$ with
$\boldsymbol{p}$ of type `ITA{T}` and a remainder $\epsilon \geq 0$ of type `T`.
For a $2\pi$-periodic function $f : \mathbb{R} \to \mathbb{R}$,
we say $f \in \boldsymbol{f}$ iff there exists a trigonometric
polynomial $p$ with $p \in \boldsymbol{p}$ such that
${\|f-p\|}_\infty \leq \epsilon$.

TAs, ITAs and RTAs can be constructed from the list of their coefficients or from a function to be interpolated (for TAs),
and converted from one type to another type

```julia
# a TA built by interpolation
julia> f = TA{Float64}(x -> exp(cos(x)))
# an ITA for x -> 3+sin(x)-5*cos(2x) with multiprecision floating-point interval coefficients
julia> g = ITA{Interval{BigFloat}}([3., 1., 0., 0., -5.])
# an RTA around g
julia> h = RTA(g, 0.1)
```

### Operations

* Evaluation at a point $x$: the result is a rigorous interval
enclosure if $x$ is an interval or if the function is given as an ITA or RTA, and a floating-point approximation otherwise .

```julia
julia> f(1.5)
# answer
julia> f(Interval(1.5))
# answer
julia> h(1.5)
# answer
```

* Arithmetic operations `+`, `-`, `*`, `/`, `^` work on TAs, ITAs and RTAs and are also defined with arguments of different types when it makes sense, e.g., adding an RTA with a TA, or scaling with a constant.

```julia
julia> f^3 # the result is a TA
julia> 1/f # the result is a TA (built by interpolation)
julia> f-g # the result is an ITA
julia> 0.7*h # the result is an RTA
julia> h/f # the result is an RTA
```

* Integrals over a period ("*sum*") and primitives ("*cumulated sum*") are computed via `sum` and `cumsum`.

```julia
julia> cumsum(f)
# answer, a numerical primitive of f
julia> sum(f)
#answer, the numerical integral of f over [0,2π]
julia> sum(h)
#answer, a rigorous interval enclosure for the integral of any function represented by h over [0,2π]
```



* The Wiener norm $||f||_{\ell^1}$ defined by

$$|a_0| + \sum_{k=1}^{N} \left(|a_k|+|b_k|\right)\geq ||f||_\infty$$


is computed by `wnorm`. In particular, for an RTA `h`, `wnorm(h)`
is a rigorous upper bound for the Wiener norm (and hence also the
infinity norm) of any function represented by `h`.

* `is_nonzero` / `is_positive` / `is_negative` checks that
functions represented by TA/ITA/RTA do not vanish / are strictly positive / are strictly negative. If the result is `true`, then the property rigorously holds, but an answer `false` *does not* mean
that the negated property necessarily holds.   



### Global parameters

There are three boolean global parameters `default_xxx` which are controlled using `set_default_xxx(flag::Bool)`:

* `default_RTA_fastmul` – if `true`, use FFT to multiply RTAs (default value is `false`).

* `default_RTA_fastmul_interval` – if `true` (default value) and
`default_RTA_fastmul=true`, use *interval* FFT to multiply RTAs. Otherwise, use **nonrigorous** floating-point FFT.

* `default_usepow2` – if `true`, always use FFT with a power of 2
(default value is `false`).


## Bibliography

**<a name="BrehardBrisebarreJoldesTucker2023">[1]</a>**
Florent Bréhard, Nicolas Brisebarre, Mioara Joldes and Warwick Tucker, *Efficient and Validated Numerical Evaluation of Abelian
Integrals*, submitted.
